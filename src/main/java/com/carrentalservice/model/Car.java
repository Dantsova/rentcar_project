package com.carrentalservice.model;

import com.carrentalservice.enums.Status;
import com.carrentalservice.eventListener.EventHandler;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "car")
@Getter
@Setter
@NoArgsConstructor

public class Car extends AbstractModel {

    private String name;

    private String modelCar;

    private LocalDate dateOfManufacture;

    private BigDecimal engineVolume;

    @Enumerated(EnumType.STRING)
    private Status status;

    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    private User owner;

}