package com.carrentalservice.model;

import com.carrentalservice.enums.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "user_data")
@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractModel {

    private String name;

    @Enumerated(EnumType.STRING)
    private Role role;

    private String email;

    private String userName;

    private String password;

    //private Date lastLogin;

    public User(String email, String userName, String password) {
        this.email = email;
        this.userName = userName;
        this.password = password;
    }
}
