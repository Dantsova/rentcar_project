package com.carrentalservice.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

public class ErrorResponseDto {
    @JsonProperty("timestamp")
    private final LocalDateTime timestamp;

    @JsonProperty("status")
    private final int httpStatus;

    @JsonProperty("error")
    private final String error;

    @JsonProperty("message")
    private final String message;

    @JsonProperty("path")
    private final String path;

    public ErrorResponseDto(LocalDateTime timestamp, int httpStatus, String error, String message, WebRequest path) {
        this.timestamp = timestamp;
        this.httpStatus = httpStatus;
        this.error = error;
        this.message = message;
        this.path = path.getContextPath();
    }
}
