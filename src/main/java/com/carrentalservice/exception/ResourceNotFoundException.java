package com.carrentalservice.exception;

public class ResourceNotFoundException extends RuntimeException {
    private String object;

    public ResourceNotFoundException(String object) {
        this.object = object;
    }

    @Override
    public String getMessage() {
        return String.format("%s is not found", object);
    }
}
