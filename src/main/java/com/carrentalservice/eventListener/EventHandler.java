package com.carrentalservice.eventListener;

import com.carrentalservice.model.History;
import com.carrentalservice.model.Lease;
import com.carrentalservice.repository.HistoryRepository;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import java.time.LocalDate;

@NoArgsConstructor
public class EventHandler {
    private HistoryRepository historyRepository;

    @PostPersist
    public void handleLeaseCreateEvent(Lease lease) {
        History history = History.builder()
                .name("Create")
                .description("")
                .lease(lease)
                .build();
        historyRepository.save(history);
    }

    @PostUpdate
    public void handleLeaseUpdateEvent(Lease lease) {
        History history = History.builder()
                .name("Update")
                .description("")
                .lease(lease).build();
        historyRepository.save(history);
    }

    @PostRemove
    public void handleLeaseDeleteEvent(Lease lease) {
        History history = History.builder()
                .name("Delete")
                .description("")
                .createdAt(LocalDate.now())
                .lease(lease).build();
        historyRepository.save(history);
    }
}