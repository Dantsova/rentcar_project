package com.carrentalservice.repository;

import com.carrentalservice.model.Lease;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface LeaseRepository extends JpaRepository<Lease, Integer> {

    @EntityGraph(attributePaths = {"car"})
    @Query("select l " +
            "from Lease l " +
            "where l.name LIKE %:name% " +
            "and l.description like %:description% " +
            "and l.createdAt >= :createdAt " +
            "and l.deadlines <= :deadlines " +
            "and l.car = :carId")
    Page<Lease> findAllByParams(String name, String description, LocalDate createdAt, LocalDate deadlines, Integer carId, Pageable pageable);
}