package com.carrentalservice.repository;

import com.carrentalservice.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {

    Page<User> findByNameAndEmailAndUserName(String name, String email, String userName, Pageable pageable);

    User findByUserName(String userName);

    User findByEmail(String name);
}