package com.carrentalservice.repository;

import com.carrentalservice.enums.Status;
import com.carrentalservice.model.Car;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car, Integer> {

    @Query(value = "select c " +
            "from Car c " +
            "where (:modelCar is null or c.modelCar like %:modelCar%) " +
            "and (c.dateOfManufacture >= :dateOfManufactureFrom AND c.dateOfManufacture <= :dateOfManufactureTo) " +
            "and (c.engineVolume between :engineVolumeFrom AND :engineVolumeTo) " +
            "and c.status IN (:statusList)" +
            "and c.owner = :ownerId")
    @EntityGraph(attributePaths = {"owner"})
    Page<Car> findAllByParams(String modelCar,
                              LocalDate dateOfManufactureFrom,
                              LocalDate dateOfManufactureTo,
                              double engineVolumeFrom,
                              double engineVolumeTo,
                              List<Status> statusList,
                              Integer ownerId,
                              Pageable pageable);
}