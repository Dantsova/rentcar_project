package com.carrentalservice.dto;

import com.carrentalservice.enums.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDto {
    private Integer id;
    @NotBlank
    private String name;
    private Role role;
    @Email(message = "email: should be valid")
    private String email;
    @NotEmpty(message = "username: Please fill out the required field")
    private String userName;
    @NotBlank(message = "password: please fill out the required field")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~.\"(),:;<>@\\[\\]!#$%&'*+-\\\\\\/=?^_`{|}]).{6,20}$",
            message = "password: pass should contain at least one uppercase and one lowercase English alpha characters, one digit and one special character and required input 6-20 characters")
    private String password;
}