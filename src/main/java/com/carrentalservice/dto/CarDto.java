package com.carrentalservice.dto;

import com.carrentalservice.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarDto {
    private Integer id;

    @NotBlank(message = "name car: mandatory field")
    private String name;

    @NotBlank(message = "model car: mandatory field")
    private String modelCar;

    private LocalDate dateOfManufacture;

    @NotNull(message = "engine volume: mandatory field")
    private BigDecimal engineVolume;

    private Status status;
    private String description;
    private Integer ownerId;
}