package com.carrentalservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
public class HistoryDto {
    private Integer id;
    @NotBlank
    private String name;
    @NotBlank
    private String description;
    private LocalDate createdAt;
    private Integer leaseId;
}