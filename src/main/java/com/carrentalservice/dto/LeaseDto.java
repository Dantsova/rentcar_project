package com.carrentalservice.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class LeaseDto {

    private Integer id;
    private String name;
    private String description;
    private LocalDate createdAt;
    private LocalDate deadlines;
    private Integer carId;
}
