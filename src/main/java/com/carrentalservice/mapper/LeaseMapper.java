package com.carrentalservice.mapper;

import com.carrentalservice.dto.LeaseDto;
import com.carrentalservice.exception.ResourceNotFoundException;
import com.carrentalservice.model.Car;
import com.carrentalservice.model.Lease;
import com.carrentalservice.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
@RequiredArgsConstructor
public abstract class LeaseMapper {
    private CarRepository carRepository;

    @Mapping(target = "car", source = "carId", qualifiedByName = "mapCar")
    public abstract Lease toModel(LeaseDto modelDTO);

    @Mapping(target = "carId", source = "car.id")
    public abstract LeaseDto toDto(Lease model);

    @Mapping(target = "id", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    public abstract void updateFromDtoToEntity(LeaseDto modelDto, @MappingTarget Lease model);

    @Named("mapCar")
    protected Car mapCar(Integer carId) {
        return carRepository
                .findById(carId)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Car %s not found", carId)));
    }
}