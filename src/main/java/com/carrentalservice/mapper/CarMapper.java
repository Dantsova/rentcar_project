package com.carrentalservice.mapper;

import com.carrentalservice.dto.CarDto;
import com.carrentalservice.exception.ResourceNotFoundException;
import com.carrentalservice.model.Car;
import com.carrentalservice.model.User;
import com.carrentalservice.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class CarMapper {
    @Autowired
    private UserRepository userRepository;

    @Mapping(target = "owner", source = "ownerId", qualifiedByName = "mapUser")
    public abstract Car toModel(CarDto modelDTO);

    @Mapping(target = "ownerId", source = "owner.id")
    public abstract CarDto toDto(Car car);

    @Mapping(target = "id", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    public abstract void updateFromDtoToEntity(CarDto carDto, @MappingTarget Car car);

    @Named("mapUser")
    protected User mapUser(Integer userId) {
        return userRepository
                .findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("User %s not found", userId)));
    }
}