package com.carrentalservice.mapper;

import com.carrentalservice.dto.HistoryDto;
import com.carrentalservice.model.History;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface HistoryMapper {

    History toModel(HistoryDto historyDto);

    HistoryDto toDto(History history);
}
