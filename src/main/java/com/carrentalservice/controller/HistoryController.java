package com.carrentalservice.controller;

import com.carrentalservice.dto.HistoryDto;
import com.carrentalservice.service.HistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequestMapping("/histories")
@RestController
@RequiredArgsConstructor
public class HistoryController {

    private final HistoryService historyService;

    @GetMapping("/{historyId}")
    public ResponseEntity<HistoryDto> get(@PathVariable(value = "historyId") Integer historyId) {
        HistoryDto historyDto = historyService.findHistoryById(historyId);
        return ResponseEntity
                .ok(historyDto);
    }

    @GetMapping
    public ResponseEntity<Page<HistoryDto>> get(Pageable pageable) {
        Page<HistoryDto> historyDtoPage = historyService.getAll(pageable);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(historyDtoPage);
    }
}
