package com.carrentalservice.controller;

import com.carrentalservice.dto.CarDto;
import com.carrentalservice.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Map;

@RequestMapping("/cars")
@RestController
@RequiredArgsConstructor
public class CarController {

    private final CarService carService;

    @PostMapping
    public ResponseEntity<CarDto> add(Principal principal,
                                      @Valid @RequestBody CarDto newCarDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header(HttpHeaders.LOCATION, "/cars/" + carService.save(principal, newCarDto).getId()).build();
    }

    @GetMapping("/{carId}")
    public ResponseEntity<CarDto> get(Principal principal,
                                      @PathVariable(value = "carId") Integer carId) {
        return ResponseEntity
                .ok(carService.findCarById(principal, carId));
    }

    @GetMapping
    public ResponseEntity<Page<CarDto>> get(Principal principal,
                                            Pageable pageable,
                                            @RequestParam Map<String, String> params) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(carService.getFilteredCars(principal, pageable, params));
    }

    @PatchMapping("/{carId}")
    public ResponseEntity<CarDto> patch(Principal principal,
                                        @RequestBody CarDto rawCarDto,
                                        @PathVariable(value = "carId") Integer carId) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(carService.patch(principal, rawCarDto, carId));
    }

    @DeleteMapping("/{carId}")
    public ResponseEntity<CarDto> delete(Principal principal,
                                         @PathVariable(value = "carId") Integer carId) {
        carService.delete(principal, carId);
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT).build();
    }
}