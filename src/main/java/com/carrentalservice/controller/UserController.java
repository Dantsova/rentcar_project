package com.carrentalservice.controller;

import com.carrentalservice.dto.UserDto;
import com.carrentalservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

@RequestMapping("/users")
@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping
    public ResponseEntity<UserDto> add(@Valid @RequestBody UserDto newUserDto) {
        UserDto createdUser = userService.save(newUserDto);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header(HttpHeaders.LOCATION, "/users/" + createdUser.getId())
                .body(createdUser);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<UserDto> get(@PathVariable(value = "userId") Integer userId) {
        UserDto userDto = userService.findUserById(userId);
        return ResponseEntity
                .ok(userDto);
    }

    @GetMapping
    public ResponseEntity<Page<UserDto>> get(Pageable pageable,
                                             @RequestParam Map<String, String> params) {
        Page<UserDto> userDtoPage = userService.getFilteredUsers(pageable, params);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userDtoPage);
    }

    @PatchMapping("/{userId}")
    public ResponseEntity<UserDto> patch(@RequestBody UserDto rawUserDto,
                                         @PathVariable(value = "userId") Integer userId) {
        UserDto userUpdated = userService.patch(rawUserDto, userId);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userUpdated);
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<UserDto> delete(@PathVariable(value = "userId") Integer userId) {
        userService.delete(userId);
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .build();
    }
}