package com.carrentalservice.controller;

import com.carrentalservice.dto.CarDto;
import com.carrentalservice.dto.LeaseDto;
import com.carrentalservice.service.LeaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

@RequestMapping("/leases")
@RestController
@RequiredArgsConstructor
public class LeaseController {
    private final LeaseService leaseService;

    @PostMapping
    @PreAuthorize(value = "hasAnyAuthority('USER')")
    public ResponseEntity<LeaseDto> add(@Valid @RequestBody LeaseDto newLeaseDto) {
        LeaseDto createdLease = leaseService.save(newLeaseDto);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header(HttpHeaders.LOCATION, "/leases/" + createdLease.getId())
                .body(createdLease);
    }

    @GetMapping("/{leaseId}")
    public ResponseEntity<LeaseDto> get(@PathVariable(value = "leaseId") Integer leaseId) {
        LeaseDto leaseDto = leaseService.findLeaseById(leaseId);
        return ResponseEntity
                .ok(leaseDto);
    }

    @GetMapping
    public ResponseEntity<Page<LeaseDto>> get(Pageable pageable,
                                              @RequestParam Map<String, String> params) {
        Page<LeaseDto> leaseDtoPage = leaseService.getFilteredLeases(pageable, params);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(leaseDtoPage);
    }

    @PatchMapping("/{leaseId}")
    @PreAuthorize(value = "hasAnyAuthority('LESSOR')")
    public ResponseEntity<LeaseDto> patch(@RequestBody LeaseDto rawLeaseDto,
                                          @PathVariable(value = "leaseId") Integer leaseId) {
        LeaseDto leaseUpdated = leaseService.patch(rawLeaseDto, leaseId);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(leaseUpdated);
    }

    @DeleteMapping("/{leaseId}")
    @PreAuthorize(value = "hasAnyAuthority('LESSOR','ADMINISTRATOR')")
    public ResponseEntity<CarDto> delete(@PathVariable(value = "leaseId") Integer leaseId) {
        leaseService.delete(leaseId);
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .build();
    }
}