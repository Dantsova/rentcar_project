package com.carrentalservice.service;

import com.carrentalservice.dto.HistoryDto;
import com.carrentalservice.exception.ResourceNotFoundException;
import com.carrentalservice.mapper.HistoryMapper;
import com.carrentalservice.model.History;
import com.carrentalservice.repository.HistoryRepository;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class HistoryService {

    private final HistoryRepository historyRepository;
    private final HistoryMapper historyMapper;
    private final Logger logger = LogManager.getLogger(this.getClass());

    public HistoryDto save(HistoryDto newHistoryDto) {
        History newHistory = historyMapper.toModel(newHistoryDto);
        newHistory.setCreatedAt(LocalDate.now());
        History savedHistory = historyRepository.save(newHistory);
        logger.info("Object of history saved");
        return historyMapper.toDto(savedHistory);
    }

    public HistoryDto findHistoryById(Integer historyId) {
        return historyMapper.toDto(historyRepository
                .findById(historyId)
                .orElseThrow(() -> new ResourceNotFoundException("History")));
    }

    public Page<HistoryDto> getAll(Pageable pageable) {
        Page<History> historyPage = historyRepository.findAll(pageable);
        logger.info("Data of history received");
        return historyPage.map(historyMapper::toDto);
    }
}