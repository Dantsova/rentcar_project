package com.carrentalservice.service;

import com.carrentalservice.dto.UserDto;
import com.carrentalservice.exception.ResourceNotFoundException;
import com.carrentalservice.mapper.UserMapper;
import com.carrentalservice.model.User;
import com.carrentalservice.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserDto save(UserDto newUserDto) {
        User newUser = userMapper.toModel(newUserDto);
        User savedUser = userRepository.save(newUser);
        return userMapper.toDto(savedUser);
    }

    public UserDto findUserById(Integer userId) {
        return userMapper.toDto(userRepository
                .findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User")));
    }

    public UserDto patch(UserDto rawUserDto, Integer userId) {
        User userToUpdate = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User"));
        userMapper.updateFromDtoToEntity(rawUserDto, userToUpdate);
        return userMapper.toDto(userToUpdate);
    }

    public Page<UserDto> getFilteredUsers(Pageable pageable, Map<String, String> requestParams) {
        String name = requestParams.get("name");
        String email = requestParams.get("email");
        String userName = requestParams.get("userName");


        Page<User> userPage = userRepository
                .findByNameAndEmailAndUserName(name, email, userName, pageable);
        return userPage.map(userMapper::toDto);
    }

    public void delete(Integer userId) {
        userRepository.deleteById(userId);
    }
}