package com.carrentalservice.service;

import com.carrentalservice.dto.CarDto;
import com.carrentalservice.enums.Status;
import com.carrentalservice.exception.ResourceNotFoundException;
import com.carrentalservice.mapper.CarMapper;
import com.carrentalservice.model.Car;
import com.carrentalservice.repository.CarRepository;
import com.carrentalservice.repository.UserRepository;
import com.carrentalservice.util.Constant;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CarService {

    private final CarMapper carMapper;
    private final CarRepository carRepository;
    private final UserRepository userRepository;
    private final Logger logger = LogManager.getLogger(this.getClass());

    public CarDto save(Principal principal, CarDto newCarDto) {
        Car newCar = carMapper.toModel(newCarDto);
        Car savedCar = carRepository.save(newCar);
        logger.info("Car {} saved", savedCar.getId());
        return carMapper.toDto(savedCar);
    }

    public CarDto findCarById(Principal principal, Integer carId) {
        return carMapper.toDto(carRepository
                .findById(carId)
                .orElseThrow(() -> new ResourceNotFoundException("Car")));
    }

    public void delete(Principal principal, Integer carId) {
        carRepository.deleteById(carId);
    }

    public CarDto patch(Principal principal, CarDto rawCarDto, Integer carId) {
        Car carToUpdate = carRepository.findById(carId)
                .orElseThrow(() -> new ResourceNotFoundException("Car"));
        logger.info("Car {} to updating found", carId);
        carMapper.updateFromDtoToEntity(rawCarDto, carToUpdate);
        logger.info("Car {} updated", carId);
        return carMapper.toDto(carToUpdate);
    }

    public Page<CarDto> getFilteredCars(Principal principal, Pageable pageable, Map<String, String> requestParams) {
       // User authUser = userRepository.findByEmail(principal.getName());
        String modelCar = requestParams.get("modelCar");
        LocalDate dateOfManufactureFrom = Optional.ofNullable(requestParams.get("dateFrom"))
                .map(rawDateFrom -> LocalDate.parse(rawDateFrom, Constant.FORMATTER))
                .orElse(Constant.MIN_DATE_OF_MANUFACTURE_CAR);
        LocalDate dateOfManufactureTo = Optional.ofNullable(requestParams.get("dateTo"))
                .map(rawDateTo -> LocalDate.parse(rawDateTo, Constant.FORMATTER))
                .orElse(LocalDate.now());
        double engineVolumeFrom = Optional.ofNullable(requestParams.get("engineVolumeFrom"))
                .map(Double::parseDouble)
                .orElse(0.0);
        double engineVolumeTo = Optional.ofNullable(requestParams.get("engineVolumeTo"))
                .map(Double::parseDouble)
                .orElse(Constant.MAX_ENGINE_VOLUME_CAR);
        var statuses = Optional.ofNullable(requestParams.get("status"))
                .map(Status::valueOf)
                .map(List::of)
                .orElse(List.of(Status.values()));
        Integer ownerId = Optional.ofNullable(requestParams.get("ownerId"))
                .map(Integer::parseInt)
                .orElse(1);//authUser.getId());
        logger.info("Request params are known");

        Page<Car> carPage = carRepository.
                findAllByParams(modelCar, dateOfManufactureFrom, dateOfManufactureTo, engineVolumeFrom, engineVolumeTo, statuses, ownerId, pageable);
        logger.info("Data cars by params received");
        return carPage.map(carMapper::toDto);
    }
}

