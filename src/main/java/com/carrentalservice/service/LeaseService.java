package com.carrentalservice.service;

import com.carrentalservice.dto.LeaseDto;
import com.carrentalservice.exception.ResourceNotFoundException;
import com.carrentalservice.mapper.LeaseMapper;
import com.carrentalservice.model.History;
import com.carrentalservice.model.Lease;
import com.carrentalservice.repository.HistoryRepository;
import com.carrentalservice.repository.LeaseRepository;
import com.carrentalservice.util.Constant;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LeaseService {
    private final LeaseRepository leaseRepository;
    private final LeaseMapper leaseMapper;
    private final HistoryRepository historyRepository;

    public LeaseDto save(LeaseDto newLeaseDto) {
        Lease newLease = leaseMapper.toModel(newLeaseDto);
        Lease savedLease = leaseRepository.save(newLease);
        return leaseMapper.toDto(savedLease);
    }

    public LeaseDto findLeaseById(Integer leaseId) {
        return leaseMapper.toDto(leaseRepository
                .findById(leaseId)
                .orElseThrow(() -> new ResourceNotFoundException("Lease")));
    }

    public void delete(Integer leaseId) {
        leaseRepository.deleteById(leaseId);
    }

    public LeaseDto patch(LeaseDto rawLeaseDto, Integer leaseId) {
        Lease leaseToUpdate = leaseRepository.findById(leaseId)
                .orElseThrow(() -> new ResourceNotFoundException("Lease"));
        leaseMapper.updateFromDtoToEntity(rawLeaseDto, leaseToUpdate);
        return leaseMapper.toDto(leaseToUpdate);
    }

    public Page<LeaseDto> getFilteredLeases(Pageable pageable, Map<String, String> requestParams) {
        String name = requestParams.get("name");
        String description = requestParams.get("description");
        LocalDate createdAt = Optional.ofNullable(requestParams.get("createdAt"))
                .map(rowCreatedAt -> LocalDate.parse(rowCreatedAt, Constant.FORMATTER))
                .orElse(Constant.MINUS_MONTH_CREATION_LEASE);
        LocalDate deadlines = Optional.ofNullable(requestParams.get("deadlines"))
                .map(rawDeadlines -> LocalDate.parse(rawDeadlines, Constant.FORMATTER))
                .orElse(Constant.PLUS_MONTH_DEADLINE_LEASE);
        Integer carId = Optional.ofNullable(requestParams.get("carId"))
                .map(Integer::parseInt)
                .orElse(1);

        Page<Lease> leasePage = leaseRepository.
                findAllByParams(name, description, createdAt, deadlines, carId, pageable);
        return leasePage.map(leaseMapper::toDto);
    }


}