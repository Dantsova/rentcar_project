package com.carrentalservice.util;

import com.carrentalservice.enums.Status;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public final class Constant {
    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("d/MM/yyyy");
    public static final LocalDate MIN_DATE_OF_MANUFACTURE_CAR = LocalDate.of(1900, 1, 1);
    public static final double MAX_ENGINE_VOLUME_CAR = 10.0;
    public static final Status DEFAULT_STATUS = Status.ACTIVE;
    public static final LocalDate MINUS_MONTH_CREATION_LEASE = LocalDate.now().minusMonths(1);
    public static final LocalDate PLUS_MONTH_DEADLINE_LEASE = LocalDate.now().plusMonths(1);

    private Constant() {
    }
}