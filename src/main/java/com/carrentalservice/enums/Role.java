package com.carrentalservice.enums;

public enum Role {
    USER,
    LESSOR,
    ADMINISTRATOR;
}