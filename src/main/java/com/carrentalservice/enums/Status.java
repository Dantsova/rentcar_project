package com.carrentalservice.enums;

public enum Status {
    ACTIVE,
    INACTIVE,
    ON_REPAIRING,
    BOOKED;
}
