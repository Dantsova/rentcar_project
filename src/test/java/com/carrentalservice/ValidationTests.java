package com.carrentalservice;

import com.carrentalservice.dto.CarDto;
import com.carrentalservice.enums.Status;
import com.carrentalservice.mapper.CarMapper;
import com.carrentalservice.model.Car;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RequiredArgsConstructor
class ValidationTests {

    private final CarMapper carMapper;

    @Test
    void shouldMapCarDtoToCar() {
        CarDto carDto = CarDto.builder()
                .name("Volkswagen Polo VI")
                .modelCar("SEDAN")
                .dateOfManufacture(LocalDate.now())
                .engineVolume(BigDecimal.valueOf(1.4))
                .status(Status.ACTIVE)
                .description("Автомат, передний привод, бензин")
                .build();
        Car car = carMapper.toModel(carDto);
        assertEquals(carDto.getName(), car.getName());
        assertEquals(carDto.getModelCar(), car.getModelCar());
        assertEquals(carDto.getDateOfManufacture(), car.getDateOfManufacture());
        assertEquals(carDto.getEngineVolume().compareTo(car.getEngineVolume()), 0);
        assertEquals(carDto.getStatus(), car.getStatus());
    }
}