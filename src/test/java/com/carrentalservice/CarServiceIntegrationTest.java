package com.carrentalservice;

import com.carrentalservice.dto.CarDto;
import com.carrentalservice.dto.UserDto;
import com.carrentalservice.enums.Role;
import com.carrentalservice.enums.Status;
import com.carrentalservice.exception.ResourceNotFoundException;
import com.carrentalservice.service.CarService;
import com.carrentalservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;

import java.math.BigDecimal;
import java.security.Principal;
import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest
@RequiredArgsConstructor
public class CarServiceIntegrationTest {
    @Autowired
    private CarService carService;

    @Autowired
    private UserService userService;

    CarDto rawCarDto, rawCarDtoForPatchStatus;
    UserDto rawOwnerCar;

    @Mock
    private Principal principal;

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = MyPostgresqlContainer.getInstance();

    @Test
    public void whenSpringContextIsBootstrapped_thenNoExceptions() {
    }

    @Before
    public void init() {
        rawOwnerCar = UserDto.builder()
                .name("Bob")
                .role(Role.ADMINISTRATOR)
                .email("BobAdmin@mail.tu")
                .userName("BobAdmin")
                .password("Az1!zA")
                .build();
        UserDto ownerCar = userService.save(rawOwnerCar);
        rawCarDto = CarDto.builder()
                .name("Audi Polo")
                .modelCar("SEDAN")
                .dateOfManufacture(LocalDate.now())
                .engineVolume(new BigDecimal("1.40"))
                .status(Status.ACTIVE)
                .description("auto, petrol")
                .ownerId(ownerCar.getId())
                .build();
        rawCarDtoForPatchStatus = CarDto.builder()
                .status(Status.INACTIVE)
                .build();
    }

    @Test
    public void givenSavedCar_WhenGetById_ThenCarAddedToDB() {
        CarDto savedCarDto = carService.save(principal, rawCarDto);
        CarDto foundCarDto = carService.findCarById(principal, savedCarDto.getId());
        Assert.assertEquals(savedCarDto, foundCarDto);
    }

    @Test
    @Transactional
    public void givenSavedCar_WhenUpdateStatus_ThenModifyMatchingCars() {
        CarDto carDto = carService.save(principal, rawCarDto);
        CarDto updatedCarDto = carService.patch(principal, rawCarDtoForPatchStatus, carDto.getId());
        CarDto foundCarDto = carService.findCarById(principal, carDto.getId());
        Assert.assertEquals(updatedCarDto.getStatus(), foundCarDto.getStatus());
    }

    @Test(expected = ResourceNotFoundException.class)
    @Transactional
    public void givenSavedCar_WhenDeleteCar_ThenCheckExc() {
        CarDto savedDto = carService.save(principal, rawCarDto);
        carService.delete(principal, savedDto.getId());
        CarDto foundCar = carService.findCarById(principal, savedDto.getId());
    }
}
