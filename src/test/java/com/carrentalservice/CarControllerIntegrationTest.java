package com.carrentalservice;

import com.carrentalservice.dto.UserDto;
import com.carrentalservice.enums.Role;
import com.carrentalservice.service.UserService;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.shaded.com.github.dockerjava.core.MediaType;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CarControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserService userService;

    private UserDto rawUser;

    @Test
    public void contextLoads() {
    }

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = MyPostgresqlContainer.getInstance();

    @Before
    public void setup() {
        rawUser = UserDto.builder()
                .name("Bob")
                .role(Role.ADMINISTRATOR)
                .email("BobAdmin@mail.tu")
                .userName("BobAdmin")
                .password("Az1!zA")
                .build();
        UserDto сar = userService.save(rawUser);
    }

    @Test
    public void shouldReturnStatus() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/cars"))
                .andExpect(status().isOk())
//                .andExpect(MockMvcResultMatchers.jsonPath("$.cars").exists())
//                .andExpect(MockMvcResultMatchers.jsonPath("$.cars[*].carId").isNotEmpty())
        ;
    }

    @Test
    public void updateCarTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .patch("/car/{id}", 1)
                .content("{\"userName\": \"Stive\"}")
                .contentType(String.valueOf(MediaType.APPLICATION_JSON))
                .accept(String.valueOf(MediaType.APPLICATION_JSON)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.userName").value("Stive"));
    }
}
