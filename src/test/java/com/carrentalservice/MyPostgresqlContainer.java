package com.carrentalservice;

import org.testcontainers.containers.PostgreSQLContainer;

public class MyPostgresqlContainer extends PostgreSQLContainer<MyPostgresqlContainer> {
    private static final String IMAGE_VERSION = "postgres";
    private static MyPostgresqlContainer container;

    private MyPostgresqlContainer() {
        super("postgres:13.2");
    }

    public static MyPostgresqlContainer getInstance() {
        if (container == null) {
            container = new MyPostgresqlContainer();
        }
        return container;
    }

    @Override
    public void start() {
        super.start();
        System.setProperty("DB_URL", container.getJdbcUrl());
        System.setProperty("DB_USERNAME", container.getUsername());
        System.setProperty("DB_PASSWORD", container.getPassword());
    }

    @Override
    public void stop() {
    }
}