package com.carrentalservice;

import com.carrentalservice.controller.CarController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class SmokeTest {
    @Autowired
    private CarController carController;

    @Test
    public void contextLoads() throws Exception {
        assertThat(carController).isNotNull();
    }
}
